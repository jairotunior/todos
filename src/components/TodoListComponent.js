import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {Button, FormControl} from 'react-bootstrap';
import {connect} from 'react-redux';

import {addTodo, toggleTodo, removeTodo, setVisibilityFilter} from '../actions/todos';
import {VisibilityFilters} from '../actions/actionTypes';

import Link from './Link';
import Todo from './Todo';

const getVisibleTodos = (todos, filter) => {
    switch(filter)
    {
        case VisibilityFilters.SHOW_ALL:
            return todos;
        case VisibilityFilters.SHOW_COMPLETED:
            return todos.filter(t => t.completed);
        case VisibilityFilters.SHOW_ACTIVE:
            return todos.filter(t => !t.completed);
        default:
            return todos;
    }
}

class TodoListComponent extends Component
{
    static propTypes = {
        todos: PropTypes.array.isRequired,
        addTodo: PropTypes.func.isRequired,
        removeTodo: PropTypes.func.isRequired,
        toggleTodo: PropTypes.func.isRequired,
        setVisibilityFilter: PropTypes.func.isRequired,
        visibilityFilter: PropTypes.string.isRequired
    }

    constructor()
    {
        super();

        this.state = {
            todo: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleAddTodo = this.handleAddTodo.bind(this);
    }

    handleChange(e)
    {
        this.setState({todo: e.target.value});
    }

    handleAddTodo()
    {
        if(this.state.todo === '')
        {
            alert("Debe tener un nombre");
            return;
        }
        this.props.addTodo(this.state.todo);
        this.setState({todo: ''});
        ReactDOM.findDOMNode(this.refs.todo).focus();
    }

    render()
    {
        const wellStyles = {maxWidth: 500, margin: '0 auto 10px'};
        const {todos, removeTodo, toggleTodo, setVisibilityFilter, visibilityFilter} = this.props;

        return(
            <div className="container">
                <h2 className="text-center text-muted">TODO APP</h2>
                <br />
                <FormControl
                    type="text"
                    value={this.state.todo}
                    onChange={this.handleChange}
                    autoFocus
                    ref='todo'
                />
                <br />
                <Button
                    bsStyle="primary"
                    bsSize="large"
                    block
                    onClick={this.handleAddTodo}
                >
                Submit
                </Button>
                <div className="well" style={wellStyles}>
                    <Link
                        onClick={() => setVisibilityFilter(VisibilityFilters.SHOW_ALL)}
                        active={VisibilityFilters.SHOW_ALL === visibilityFilter}
                    >
                        <span>ALL</span>
                    </Link>
                    <Link
                        onClick={() => setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED)}
                        active={VisibilityFilters.SHOW_COMPLETED === visibilityFilter}
                    >
                        <span>COMPLETED</span>
                    </Link>
                    <Link
                        onClick={() => setVisibilityFilter(VisibilityFilters.SHOW_ACTIVE)}
                        active={VisibilityFilters.SHOW_ACTIVE === visibilityFilter}
                    >
                        <span>ACTIVE</span>
                    </Link>
                </div>
                <ul>
                    {todos.map(todo => 
                        <Todo
                            key={todo.id}
                            onClick={() => toggleTodo(todo.id)}
                            onDelete={() => removeTodo(todo.id)}
                            todo={todo}
                        />
                    )}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    todos: getVisibleTodos(state.todos, state.setVisibilitiFilter),
    visibilityFilter: state.setVisibilitiFilter
});

const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (todo) => {
            dispatch(addTodo(todo));
        },
        toggleTodo: (id) => {
            dispatch(toggleTodo(id));
        },
        removeTodo: (id) => {
            dispatch(removeTodo(id));
        },
        setVisibilityFilter: (filter) => {
            dispatch(setVisibilityFilter(filter));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoListComponent);