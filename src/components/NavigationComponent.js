import React, { Component } from 'react';
import { NavItem, Navbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class NavigationComponent extends Component
{
    render()
    {
        return(
            <Navbar className="container" inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a>React-Redux-Router</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to=''>
                            <NavItem eventKey={1}>Home</NavItem>
                        </LinkContainer>
                        <LinkContainer to='/counter'>
                            <NavItem eventKey={2}>Counter</NavItem>
                        </LinkContainer>
                        <LinkContainer to='/todos'>
                            <NavItem eventKey={3}>Todos</NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default NavigationComponent;