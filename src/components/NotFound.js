import React, { Component } from 'react';


class NotFound extends Component
{
    render()
    {
        return(
            <div>
                <h2 className = "text-center text-muted">La ruta no ha sido encontrada</h2>
            </div>
        )
    }
}

export default NotFound;