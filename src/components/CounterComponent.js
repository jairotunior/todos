import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, FormControl } from 'react-bootstrap';
import { increment, decrement } from '../actions/counter';
import { connect } from 'react-redux';

class CounterComponent extends Component
{
    static propTypes = {
        counter: PropTypes.number.isRequired,
        increment: PropTypes.func.isRequired,
        decrement: PropTypes.func.isRequired
    }

    render()
    {
        const wellStyles = { maxWidth: 400, margin: '0 auto 10px'};
        const { counter, increment, decrement } = this.props;

        return (
            <div>
                <h1 className='text-center text-muted'>Counter React Redux</h1>
                <div className='well' style={wellStyles}>
                    <Button
                        bsStyle = "primary"
                        bsSize = "large"
                        block
                        onClick={increment}
                    >increment</Button>
                    <br />
                    <FormControl
                        type = "text"
                        value = {counter}
                        disabled
                    />
                    <br />
                    <Button
                        bsStyle = "danger"
                        block
                        onClick = {decrement}
                    >Decrement</Button>
                </div>
            </div>
        )
    }
}

// Convierte el estado en propiedades
const mapStateToProps = state => ({
    counter: state.counter.count
})

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => {
            dispatch(increment());
        },
        decrement: () => {
            dispatch(decrement());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent);