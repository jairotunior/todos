import * as types from './actionTypes';

let nextToId = 0;

export function addTodo(text)
{
    return {
        type: types.ADD_TODO,
        id: nextToId++,
        text
    }
}

export function removeTodo(id)
{
    return {
        type: types.REMOVE_TODO,
        id
    }
}

export function toggleTodo(id)
{
    return {
        type: types.TOGGLE_TODO,
        id
    }
}

export function setVisibilityFilter(filter)
{
    return {
        type: types.SET_VISIBILITY_FILTER,
        filter
    }
}