import { combineReducers } from 'redux';

import counter from './counter';
import todos from './todos';
import setVisibilitiFilter from './setVisibilityFilter';

const rootReducer = combineReducers({
    counter,
    todos,
    setVisibilitiFilter
});

export default rootReducer;