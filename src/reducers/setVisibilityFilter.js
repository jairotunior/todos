import * as types from '../actions/actionTypes';
import {VisibilityFilters} from '../actions/actionTypes';

export default function reducer(state = VisibilityFilters.SHOW_ALL, action)
{
    switch(action.type)
    {
        case types.SET_VISIBILITY_FILTER:
        {
            return action.filter;
        }
        default:
            return state;
    }
}