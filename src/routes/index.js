import React from 'react';
import { Route, Switch } from 'react-router';
import Home from '../components/HomeComponent';
import NotFound from '../components/NotFound';
import NavigationComponent from '../components/NavigationComponent';
import CounterComponent from '../components/CounterComponent';
import TodoListComponent from '../components/TodoListComponent';

const routes = (
    <div>
        <NavigationComponent />
        <Switch>
            <Route exact path = "/" component = {Home}/>
            <Route path = "/counter" component = {CounterComponent}/>
            <Route path="/todos" component={TodoListComponent} />
            <Route component = {NotFound}/>
        </Switch>
    </div>
)

export default routes;